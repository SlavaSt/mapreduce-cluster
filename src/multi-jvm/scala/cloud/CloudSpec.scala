package cloud

import akka.actor.Props
import akka.cluster.Cluster
import akka.remote.testkit.{MultiNodeConfig, MultiNodeSpec}
import akka.testkit.ImplicitSender
import cloud.CloudServiceMsg._
import cloud.functions.{MapFunction, ReduceFunction}
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.language.postfixOps

object CloudSpecConfig extends MultiNodeConfig {

  val clusterNode1 = role("clusterNode1")
  val clusterNode2 = role("clusterNode2")
  val reduceNode = role("reduceNode")
  val frontend = role("frontend")

  commonConfig(ConfigFactory.parseString( """
      akka.loglevel = INFO
      akka.actor.provider = "akka.cluster.ClusterActorRefProvider"
      akka.remote.log-remote-lifecycle-events = off
      akka.cluster.metrics.collector-class = akka.cluster.JmxMetricsCollector

                                          """))

  nodeConfig(clusterNode1)(
    ConfigFactory.parseString("akka.cluster.roles = [clusterNode]")
  )

  nodeConfig(frontend)(
    ConfigFactory.parseString("akka.cluster.roles = [frontEnd]")
  )
}

class CloudSpecMultiJvmClusterNode1 extends CloudSpec

class CloudSpecMultiJvmClusterNode2 extends CloudSpec

class CloudSpecMultiJvmCloudService extends CloudSpec {
  "The CloudService" must {

    "start cloudService" in within(10 seconds) {
      runOn(frontend) {
        awaitAssert {
          val cloudService = system.actorSelection("akka://" + system.name + "/user/cloudService")
          cloudService ! new IsRunning
          expectMsgType[Boolean](1 second) should be(true)
        }
        cloudService ! new SubscribeToClusterEvents
        expectMsgPF(max = 10 second) {
          case msg: MemberUp => // member is up
        }
        cloudService ! new Save(null, 1, 1)
        cloudService ! new Save(null, 2, 2)
        cloudService ! new Save(null, 3, 3)
        cloudService ! new CloudServiceMsg.MapReduce[util.Collection[Int], Int, Int](
          mapFun,
          reduceFun,
          0
        )
        expectMsgPF(max = 10 seconds) {
          case memberUp: MemberUp => // ok
          case result: Int => println("MapReduce result: " + result)
        }
      }
      testConductor.enter("done")
    }

    "done" in within(10 seconds) {
      runOn(frontend) {
        testConductor.enter("done")
      }
    }
  }
}

abstract class CloudSpec extends MultiNodeSpec(CloudSpecConfig) with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {

  "All nodes in the Cloud" must {

    "wait for all nodes to enter a barrier" in {
      enterBarrier("startup")
    }

    "join the cluster (frontend)" in within(10 seconds) {
      runOn(frontend) {
        Cluster(system) join node(frontend).address
        system.actorOf(Props[CloudService], name = "cloudService")
        //Thread.sleep(2000)
      }

      runOn(clusterNode1) {
        Cluster(system) join node(frontend).address
        system.actorOf(Props[ClusterNode], name = "clusterNode")
        testConductor.enter("clusterNode1-started")
      }

      runOn(frontend) {
        testConductor.enter("clusterNode1-started")
        cloudService ! new SubscribeToClusterEvents
        expectMsgPF(max = 10 second) {
          case msg: MemberUp => // member is up
        }
      }

      runOn(frontend) {
        //testConductor.enter("clusterNode1.started")
        cloudService ! new Save(1, 1)
        cloudService ! new Save(2, 2)
        cloudService ! new Save(3, 3)
      }
    }

    "join the cluster (clusterNode2)" in within(10 seconds) {
      runOn(clusterNode2) {
        Cluster(system) join node(clusterNode1).address
        system.actorOf(Props[ClusterNode], name = "clusterNode")
        //Thread.sleep(2000)
      }
      testConductor.enter("clusterNode2.started")
    }

  }

  override def initialParticipants = roles.size

  override def beforeAll() = multiNodeSpecBeforeAll()

  override def afterAll() = multiNodeSpecAfterAll()

  def cloudService = system.actorSelection("akka://" + system.name + "/user/cloudService")

  val mapFun: MapFunction[java.util.Collection[Int], Int] = new MapFunction[java.util.Collection[Int], Int] {
    override def apply(elems: java.util.Collection[Int]) = elems.iterator().next()
  }

  val reduceFun: ReduceFunction[Int, Int] = new ReduceFunction[Int, Int] {
    override def apply(zero: Int, elem: Int) = zero + elem
  }

}

