/* 9/26/14 */
package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Slava Stashuk
 */
public class Document implements Serializable {

    private final UUID uuid = UUID.randomUUID();

    private final BigDecimal amount;

    private final int group;

    private final LocalDateTime dateTime;

    public Document(BigDecimal amount, int group, LocalDateTime dateTime) {
        this.amount = amount;
        this.group = group;
        this.dateTime = dateTime;
    }

    public UUID getId() {
        return uuid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public int getGroup() {
        return group;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "Document{" +
                "amount=" + amount +
                '}';
    }
}
