/* 9/26/14 */
package domain;

import cloud.CloudServiceFrontend;
import cloud.common.Properties;
import cloud.common.SerializablePredicate;
import cloud.functions.MapFunction;
import cloud.functions.ReduceFunction;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * @author Slava Stashuk
 */
public class DocumentService {

    private CloudServiceFrontend cloudService;

    public DocumentService(CloudServiceFrontend cloudService) {
        this.cloudService = cloudService;
    }

    public BigDecimal getTotalAmount() {
        return getAmount(doc -> true);
    }

    public BigDecimal getAmountByGroup(int group) {
        return getAmount(doc -> doc.getGroup() == group);
    }

    public BigDecimal getAmountForPeriod(LocalDateTime from, LocalDateTime to) {
        return getAmount(doc -> doc.getDateTime().isAfter(from) && doc.getDateTime().isBefore(to));
    }

    private BigDecimal getAmount(SerializablePredicate<Document> predicate) {
        try {
            return Await.result(getAmountFuture(predicate), Properties.TIMEOUT_DURATION);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Future<BigDecimal> getAmountFuture(SerializablePredicate<Document> predicate) {
        return cloudService.mapReduce(
                    mapFun(predicate),
                    reduceFun(),
                    BigDecimal.ZERO
        );
    }

    protected static MapFunction<Collection<Document>, BigDecimal> mapFun(SerializablePredicate<Document> predicate) {
        return documents -> documents.stream()
                .filter(predicate)
                .map(Document::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private ReduceFunction<BigDecimal, BigDecimal> reduceFun() {
        return (amount1, amount2) -> amount1.add(amount2);
    }
}
