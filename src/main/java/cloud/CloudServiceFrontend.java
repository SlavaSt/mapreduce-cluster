/* 9/27/14 */
package cloud;

import cloud.functions.MapFunction;
import cloud.functions.ReduceFunction;
import scala.concurrent.Future;

/**
 * @author Slava Stashuk
 */
public interface CloudServiceFrontend {

    <E, T, U> Future<U> mapReduce(MapFunction<E, T> mapFun, ReduceFunction<T, U> reduceFun, U identity);

}
