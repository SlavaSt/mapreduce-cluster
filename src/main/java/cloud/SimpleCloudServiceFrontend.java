/* 9/27/14 */
package cloud;

import akka.actor.ActorRef;
import cloud.common.Properties;
import cloud.functions.MapFunction;
import cloud.functions.ReduceFunction;
import scala.concurrent.Future;

/**
 * @author Slava Stashuk
 */
public class SimpleCloudServiceFrontend implements CloudServiceFrontend {

    private final ActorRef cloudService;

    public SimpleCloudServiceFrontend(ActorRef cloudService) {
        this.cloudService = cloudService;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <E, T, U> Future<U> mapReduce(MapFunction<E, T> mapFun, ReduceFunction<T, U> reduceFun, U identity) {
        return (Future<U>) akka.pattern.Patterns.ask(cloudService, new CloudServiceMsg.MapReduce<>(mapFun, reduceFun, identity), Properties.TIMEOUT);
    }

}
