/* 9/29/14 */
package cloud.input;

import domain.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Slava Stashuk
 */
public class TestInput {

    public static List<Document> getDocuments() {
        List<Document> documents = new LinkedList<>();
        for (int i = 0; i <= 1000000; i++) {
            documents.add(new Document(BigDecimal.valueOf(i), 1, LocalDateTime.now()));

        }
        return documents;
    }
}
