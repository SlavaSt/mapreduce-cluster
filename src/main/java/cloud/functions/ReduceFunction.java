/* 9/23/14 */
package cloud.functions;

import java.io.Serializable;
import java.util.function.BiFunction;

/**
 * @author Slava Stashuk
 */
public interface ReduceFunction<V, R> extends BiFunction<V, R, V>, Serializable {
}
