/* 9/23/14 */
package cloud.functions;

import java.io.Serializable;
import java.util.function.Function;

/**
 * @author Slava Stashuk
 */
public interface MapFunction<V, R> extends Function<V, R>, Serializable {
}
