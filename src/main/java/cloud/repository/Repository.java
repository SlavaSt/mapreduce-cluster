package cloud.repository;/* 9/23/14 */

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Slava Stashuk
 */
public class Repository<K, V> {

    private Map<K, V> store = new ConcurrentHashMap<>();

    public void put(K key, V value) {
        V oldValue = store.putIfAbsent(key, value);
        if (oldValue != null) {
            throw new RuntimeException("Object with key " + key + " is already in the store.");
        }
    }

    public void putAll(Map<K, V> values) {
        for (Map.Entry<K, V> kvEntry : values.entrySet()) {
            put(kvEntry.getKey(), kvEntry.getValue());
        }
    }

    public List<V> getAll() {
        return ImmutableList.copyOf(store.values());
    }


    // TODO is it needed now?
    public List<V> query(Predicate<V> predicate) {
        return store.values().stream().filter(predicate).collect(Collectors.toList());
    }

}
