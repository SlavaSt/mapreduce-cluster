package cloud.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author Slava Stashuk
 */
public interface RepositoryMsg {

    class Put<K, V> implements Serializable {
        private K key;
        private V value;

        public Put(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "Put{" +
                    "value=" + value +
                    '}';
        }
    }

    class GetAll implements Serializable {
    }

    class GetAllResult<V> implements Serializable {
        private List<V> result;

        public GetAllResult(List<V> result) {
            this.result = result;
        }
    }

    static class Ack implements Serializable {
    }

    class PutAll<K, V> implements Serializable {
        private final Map<K, V> values;

        public PutAll(Map<K, V> values) {
            this.values = values;
        }

        public Map<K, V> getValues() {
            return values;
        }
    }
}
