/* 9/24/14 */
package cloud.map;

import cloud.common.Actor;
import cloud.common.annotation.Listener;
import cloud.functions.MapFunction;
import cloud.repository.Repository;

import java.util.List;

/**
 * @author Slava Stashuk
 */
public class MapService extends Actor {


    private Repository repository;

    public MapService(Repository repository) {
        this.repository = repository;
    }

    @Listener
    public MapMsg.Result map(MapMsg.MapTask mapTask) {
        List<?> elements = repository.getAll();
        context().stop(self());
        return new MapMsg.Result<>(map(mapTask.mapFunction(), elements));
    }

    private <V, R> R map(MapFunction<List<V>, R> mapFun, List<V> elements) {
        return mapFun.apply(elements);
    }

}
