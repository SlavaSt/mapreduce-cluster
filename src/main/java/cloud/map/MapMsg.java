package cloud.map;

import cloud.functions.MapFunction;

import java.io.Serializable;

/**
 * @author Slava Stashuk
 */
public interface MapMsg {

    class MapTask<V, R> implements Serializable {
        private final MapFunction<V, R> mapFun;

        public MapTask(MapFunction<V, R> mapFun) {
            this.mapFun = mapFun;
        }

        public MapFunction<V, R> mapFunction() {
            return mapFun;
        }
    }

    class Result<V> implements Serializable {
        private final V result;

        public Result(V result) {
            this.result = result;
        }

        public V getResult() {
            return result;
        }
    }
}
