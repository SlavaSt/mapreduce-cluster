/* 9/28/14 */
package cloud;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

/**
 * @author Slava Stashuk
 */
public class ClusterNodeApp extends CloudApp {

    public static void main(String[] args) {
        ActorSystem system = actorSystem("clusterNode", 2551, 2552, 0);
        System.out.println(system);

        ActorRef clusterNode = clusterNodeIn(system);
    }

}
