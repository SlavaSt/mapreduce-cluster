package cloud;

import akka.actor.ActorRef;
import cloud.functions.MapFunction;
import cloud.functions.ReduceFunction;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Slava Stashuk
 */
public interface CloudServiceMsg {

    class MapReduce<E, T, U> implements Serializable {
        private final MapFunction<E, T> mapFun;
        private final ReduceFunction<T, U> reduceFun;
        private final U identity;

        public MapReduce(MapFunction<E, T> mapFun, ReduceFunction<T, U> reduceFun, U identity) {
            this.mapFun = mapFun;
            this.reduceFun = reduceFun;
            this.identity = identity;
        }

        public MapFunction<E, T> getMapFunction() {
            return mapFun;
        }

        public ReduceFunction<T, U> getReduceFunction() {
            return reduceFun;
        }

        public U getIdentity() {
            return identity;
        }
    }

    class AddClusterNode implements Serializable {

        private final ActorRef clusterNode;

        public AddClusterNode(ActorRef clusterNode) {
            this.clusterNode = clusterNode;
        }

        public ActorRef getClusterNode() {
            return clusterNode;
        }
    }

    public class SaveAll<K, V> implements Serializable {
        private final Map<K, V> values;

        public SaveAll(Map<K, V> values) {
            this.values = values;
        }

        public Map<K, V> getValues() {
            return values;
        }
    }

    public class Save implements Serializable {
        private Object key;
        private Object value;

        public Save(Object key, Object value) {
            this.key = key;
            this.value = value;
        }

        public Object key() {
            return key;
        }

        public Object value() {
            return value;
        }
    }

    public class IsRunning implements Serializable {}

    public class SubscribeToClusterEvents implements Serializable {}

    public class GetClusterState implements Serializable {}

    public class ClusterState implements Serializable {}

    public class MemberUp implements Serializable {
        public final ActorRef actorRef;
        public final int totalNodesCount;

        public MemberUp(ActorRef actorRef, int totalNodesCount) {
            this.actorRef = actorRef;
            this.totalNodesCount = totalNodesCount;
        }

        @Override
        public String toString() {
            return "MemberUp{" +
                    "actorRef=" + actorRef +
                    '}';
        }
    }
}
