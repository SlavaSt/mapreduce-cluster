package cloud;/* 9/26/14 */

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.jboss.netty.channel.ChannelException;

import java.util.Arrays;

/**
 * @author Slava Stashuk
 */
public abstract class CloudApp {


    protected static ActorSystem actorSystem(String role, int... ports) {
        for (int port : ports) {
            try {
                Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port)
                        .withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + role + "]"))
                        .withFallback(ConfigFactory.load());
                return ActorSystem.create("ClusterSystem", config);
            } catch (ChannelException bindException) {
                // let's try the next port
            }
        }
        throw new ChannelException("Failed to bind to any of " + Arrays.toString(ports));
    }

    protected static ActorRef clusterNodeIn(ActorSystem system) {
        return system.actorOf(Props.create(ClusterNode.class), "clusterNode");
    }

    protected static ActorRef cloudServiceIn(ActorSystem system) {
        return system.actorOf(Props.create(CloudService.class), "cloudService");
    }

}
