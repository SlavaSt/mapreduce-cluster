/* 9/24/14 */
package cloud.mapreduce;

import cloud.functions.MapFunction;
import cloud.functions.ReduceFunction;

import java.io.Serializable;

/**
 * @author Slava Stashuk
 */
public interface MapReduceMsg {

    class MapReduce<E, T, U> implements Serializable {
        private final MapFunction<E, T> mapFun;
        private final ReduceFunction<T, U> reduceFun;
        private final U identity;

        public MapReduce(MapFunction<E, T> mapFun, ReduceFunction<T, U> reduceFun, U identity) {
            this.mapFun = mapFun;
            this.reduceFun = reduceFun;
            this.identity = identity;
        }

        public MapFunction<E, T> getMapFunction() {
            return mapFun;
        }

        public ReduceFunction<T, U> getReduceFunction() {
            return reduceFun;
        }

        public U getIdentity() {
            return identity;
        }
    }

    class Result<V> implements Serializable {
        public Result(V result) {
            this.result = result;
        }

        private final V result;
    }
}
