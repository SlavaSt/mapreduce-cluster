/* 9/24/14 */
package cloud.mapreduce;

import akka.actor.ActorRef;
import cloud.common.Actor;
import cloud.common.annotation.Listener;
import cloud.functions.MapFunction;
import cloud.map.MapMsg;
import cloud.reduce.ReduceMsg;
import cloud.reduce.ReduceService;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Slava Stashuk
 */
public class MapReduceJob extends Actor {

    private final Collection<ActorRef> clusterNodes;

    private ActorRef reduceService;

    private AtomicInteger reduceCounter;

    private ActorRef requester;

    public MapReduceJob(Collection<ActorRef> clusterNodes) {
        this.clusterNodes = clusterNodes;
        this.reduceCounter = new AtomicInteger(clusterNodes.size());
    }

    @Listener
    public <E, T, U> void mapReduce(MapReduceMsg.MapReduce<E, T, U> mapReduce) {
        requester = sender();
        MapFunction<E, T> mapFun = mapReduce.getMapFunction();
        reduceService = newReduceService(mapReduce);

        for (ActorRef mapService : clusterNodes) {
            mapService.tell(new MapMsg.MapTask<>(mapFun), self());
        }
    }

    @Listener
    public void handleMapJobCompleted(MapMsg.Result mapResult) {
        reduceService.tell(new ReduceMsg.NewValue(mapResult.getResult()), self());
    }

    @Listener
    public void handleReduceStepCompleted(ReduceMsg.Ack reduceJobAck) {
        if (reduceCounter.decrementAndGet() == 0) {
            reduceService.tell(new ReduceMsg.GetResult(), self());
        }
    }

    @Listener
    public void handleReduceJobcompleted(ReduceMsg.Result reduceResult) {
        requester.tell(reduceResult.getResult(), self());
        context().stop(self());
    }

    private <E, T, U> ActorRef newReduceService(MapReduceMsg.MapReduce<E, T, U> mapReduce) {
        return actorOf(ReduceService.class, () -> new ReduceService(mapReduce.getReduceFunction(), mapReduce.getIdentity()));
    }

}
