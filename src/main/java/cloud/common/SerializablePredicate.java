/* 9/29/14 */
package cloud.common;

import java.io.Serializable;
import java.util.function.Predicate;

/**
 * @author Slava Stashuk
 */
public interface SerializablePredicate<T> extends Predicate<T>, Serializable {
}
