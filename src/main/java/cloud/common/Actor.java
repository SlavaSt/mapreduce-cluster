/* 9/27/14 */
package cloud.common;

import akka.actor.ActorInitializationException;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Creator;
import cloud.common.annotation.Listener;
import com.google.common.collect.Maps;
import com.google.common.reflect.Invokable;
import scala.concurrent.Future;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

/**
 * @author Slava Stashuk
 */
public abstract class Actor extends UntypedActor {

    private final List<Method> listeners = stream(getClass().getMethods())
            .filter(method -> method.getAnnotation(Listener.class) != null)
            .collect(Collectors.toList());

    {
        if (listeners.stream().map(Method::getParameterTypes).filter(parameterTypes -> parameterTypes.length != 1).count() != 0) {
            throw new ActorInitializationException(self(), "Methods annotated with @Listener should have exactly one argument", null);
        }
    }

    private final Map<Class<?>, Method> listenersMap = Maps.uniqueIndex(listeners, method -> method.getParameters()[0].getType());

    protected final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public final void onReceive(Object message) throws Exception {
        log.info("CS: {}", message);  // TODO change to debug
        List<Method> compatibleListener = listenersMap.entrySet().stream().filter(classMethod -> classMethod.getKey() == message.getClass())
                .map(Map.Entry::getValue).collect(Collectors.toList());
        assert compatibleListener.size() <= 1;

        if (compatibleListener.size() == 1) {
            Invokable listenerMethod = Invokable.from(compatibleListener.get(0));
            @SuppressWarnings("unchecked")
            Object returnedValue = listenerMethod.invoke(this, message);
            if (returnedValue != null) {
                sender().tell(returnedValue, self());
            }
        } else {
            unhandled(message);
        }
    }

    public <T extends akka.actor.Actor> ActorRef actorOf(Class<T> actorClass, Creator<T> creator) {
        return context().actorOf(Props.create(actorClass, () -> creator.create()));
    }

    public <T extends akka.actor.Actor> ActorRef actorOf(Class<T> actorClass, Creator<T> creator, String name) {
        return context().actorOf(Props.create(actorClass, () -> creator.create()), name);
    }

    public Future<?> ask(ActorRef actorRef, Serializable msg) {
        return akka.pattern.Patterns.ask(actorRef, msg, Properties.TIMEOUT);
    }

    public void pipeToSender(Future<?> future) {
        akka.pattern.Patterns.pipe(future, context().dispatcher()).to(sender());
    }

}
