package cloud.common;

import akka.util.Timeout;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * @author Slava Stashuk
 */
public interface Properties { // TODO make configurableAdd

    Timeout TIMEOUT = new Timeout(new FiniteDuration(10, TimeUnit.MINUTES));

    Duration TIMEOUT_DURATION = Duration.apply(10, TimeUnit.MINUTES);

    FiniteDuration FINITE_DURATION = FiniteDuration.apply(10, TimeUnit.MINUTES);

}
