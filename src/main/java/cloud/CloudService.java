/* 9/26/14 */
package cloud;

import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.Identify;
import akka.actor.Terminated;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import cloud.common.Actor;
import cloud.common.annotation.Listener;
import cloud.mapreduce.MapReduceJob;
import cloud.mapreduce.MapReduceMsg;
import cloud.repository.RepositoryMsg;

import java.util.*;

import static akka.cluster.ClusterEvent.MemberUp;

/**
 * @author Slava Stashuk
 */
public class CloudService extends Actor {

    private final Cluster cluster = Cluster.get(context().system());

    private Set<UUID> registrationIds = new HashSet<>();

    private List<ActorRef> clusterNodes = new ArrayList<>();

    private List<ActorRef> clusterEventListeners = new ArrayList<>();

    @Override
    public void preStart() {
        log.info("CS:CloudService.preStart");
        cluster.subscribe(self(), MemberUp.class);
    }

    @Override
    public void postStop() {
        cluster.unsubscribe(getSelf());
    }


    @Listener
    public void mapReduce(CloudServiceMsg.MapReduce mapReduce) {
        ActorRef mapReduceJob = actorOf(MapReduceJob.class, () -> new MapReduceJob(clusterNodes));
        mapReduceJob.forward(newMapReduceMsg(mapReduce), context());
    }

    @Listener
    public void addClusterNode(CloudServiceMsg.AddClusterNode addClusterNode) {
        clusterNodes.add(addClusterNode.getClusterNode());
    }

    @Listener
    public void save(CloudServiceMsg.Save save) {
        pipeToSender(ask(randomClusterNode(), new RepositoryMsg.Put(save.key(), save.value())));
    }

    @Listener
    public void saveAll(CloudServiceMsg.SaveAll saveAll) {
        pipeToSender(ask(randomClusterNode(), new RepositoryMsg.PutAll(saveAll.getValues())));
    }

    @Listener
    public boolean isRunning(CloudServiceMsg.IsRunning isRunning) {
        return true;
    }

    @Listener
    public void subscribeToClusterEvents(CloudServiceMsg.SubscribeToClusterEvents subscribeToClusterEvents) {
        clusterEventListeners.add(sender());
    }

    @Listener
    public CloudServiceMsg.ClusterState getClusterState(CloudServiceMsg.GetClusterState getClusterState) {
        return new CloudServiceMsg.ClusterState();
    }

    @Listener
    public void handleMemberUp(MemberUp memberUp) {
        log.info("CS:CloudService.handleMemberUp");
        log.info("CS:" + memberUp.member().roles());
        requestClusterNodeIdentity(memberUp.member());
    }

    @Listener
    public void handleMemberTerminated(Terminated terminated) {
        log.info("CS:CloudService.handleMemberTerminated");
        clusterNodes.remove(terminated.actor());
    }

    @Listener
    public void registerInitialNodes(ClusterEvent.CurrentClusterState currentClusterState) {
        log.info("CS:CloudService.registerInitialNodes");
        for (Member member : currentClusterState.getMembers()) {
            if (!member.status().equals(MemberStatus.up())) continue;
            log.info("CS:" + member.roles());
            requestClusterNodeIdentity(member);
        }
    }

    @Listener
    public void registerNewClusterNode(ActorIdentity actorIdentity) {
        log.info("CS:CloudService.registerNewClusterNode");
        if (actorIdentity.getRef() == null) return;
        if (actorIdentity.correlationId() instanceof UUID) {
            UUID registrationId = (UUID) actorIdentity.correlationId();
            if (registrationIds.contains(registrationId)) {
                registrationIds.remove(registrationId);
                clusterNodes.add(actorIdentity.getRef());
                context().watch(actorIdentity.getRef());
                for (ActorRef clusterEventListener : clusterEventListeners) {
                    clusterEventListener.tell(new CloudServiceMsg.MemberUp(actorIdentity.getRef(), clusterNodes.size()), self());
                }
            }
        }
    }

    private void requestClusterNodeIdentity(Member member) {
        if (member.hasRole("clusterNode")) {
            UUID registrationId = UUID.randomUUID();
            registrationIds.add(registrationId);
            context().actorSelection(member.address() + "/user/clusterNode")
                    .tell(new Identify(registrationId), self());
        }
    }

    private MapReduceMsg.MapReduce newMapReduceMsg(CloudServiceMsg.MapReduce mapReduce) {
        return new MapReduceMsg.MapReduce(mapReduce.getMapFunction(), mapReduce.getReduceFunction(), mapReduce.getIdentity());
    }

    private ActorRef randomClusterNode() {
        return clusterNodes.get(new Random().nextInt(clusterNodes.size()));
    }

}
