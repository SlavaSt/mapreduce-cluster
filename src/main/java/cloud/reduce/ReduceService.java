/* 9/24/14 */
package cloud.reduce;

import cloud.common.Actor;
import cloud.common.annotation.Listener;
import cloud.functions.ReduceFunction;

/**
 * @author Slava Stashuk
 */
public class ReduceService extends Actor {

    private final ReduceFunction reduceFun;
    private Object result;

    public <T, U> ReduceService(ReduceFunction<T, U> reduceFun, U zero) {
        this.reduceFun = reduceFun;
        this.result = zero;
    }

    @Listener
    public ReduceMsg.Ack acceptValue(ReduceMsg.NewValue newValue) {
        Object value = newValue.getValue();
        result = reduceFun.apply(result, value);
        return new ReduceMsg.Ack();
    }

    @Listener
    public ReduceMsg.Result getResult(ReduceMsg.GetResult getResult) {
        context().stop(self());
        return new ReduceMsg.Result(result);
    }

}
