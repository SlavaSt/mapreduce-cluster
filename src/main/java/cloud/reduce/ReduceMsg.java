/* 9/24/14 */
package cloud.reduce;

/**
 * @author Slava Stashuk
 */
public interface ReduceMsg {

    class NewValue<V> {

        private V value;

        public NewValue(V value) {
            this.value = value;
        }

        public V getValue() {
            return value;
        }
    }

    class GetResult {
    }

    class Result<V> {

        private final V result;

        public Result(V result) {
            this.result = result;
        }

        public V getResult() {
            return result;
        }
    }

    class Ack {

    }
}
