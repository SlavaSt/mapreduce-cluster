/* 9/27/14 */
package cloud;

import akka.actor.ActorRef;
import cloud.common.Actor;
import cloud.common.annotation.Listener;
import cloud.map.MapMsg;
import cloud.map.MapService;
import cloud.repository.Repository;
import cloud.repository.RepositoryMsg;

/**
 * @author Slava Stashuk
 */
public class ClusterNode extends Actor {

    private Repository repository = new Repository();

    @Listener
    public RepositoryMsg.Ack save(RepositoryMsg.Put put) {
        repository.put(put.getKey(), put.getValue());
        return new RepositoryMsg.Ack();
    }

    @Listener
    public RepositoryMsg.Ack saveAll(RepositoryMsg.PutAll putAll) {
        repository.putAll(putAll.getValues());
        return new RepositoryMsg.Ack();
    }

    @Listener
    public void map(MapMsg.MapTask mapTask) {
        newMapService().forward(mapTask, context());
    }

    private ActorRef newMapService() {
        return actorOf(MapService.class, () -> new MapService(repository));
    }


}
