/* 9/29/14 */
package cloud;

import akka.actor.*;
import cloud.common.Properties;
import cloud.input.TestInput;
import cloud.repository.RepositoryMsg;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import domain.Document;
import domain.DocumentService;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Slava Stashuk
 */
public class CloudServiceApp extends CloudApp {

    private final static ActorSystem system = actorSystem("frontend", 2551, 2552, 0);

    public static void main(String[] args) throws InterruptedException, IOException {
        ActorRef cloudService = cloudServiceIn(system);

        awaitTwoClusterNodesToJoin(cloudService);

        List<Document> sampleDocuments = TestInput.getDocuments();


        Inbox saveAckInbox = Inbox.create(system);
        List<List<Document>> chunks = Lists.partition(sampleDocuments, 1000);
        new Thread(() -> {
            for (List<Document> chunk : chunks) {
                cloudService.tell(new CloudServiceMsg.SaveAll<>(Maps.uniqueIndex(chunk, Document::getId)), saveAckInbox.getRef());
            }
        }).start();

        allDocumentsToBeSaved(chunks.size(), saveAckInbox).await();

        CloudServiceFrontend frontEnd =
                TypedActor.get(system).typedActorOf(
                        new TypedProps<SimpleCloudServiceFrontend>(CloudServiceFrontend.class, () -> new SimpleCloudServiceFrontend(cloudService))
                );

        DocumentService documentService = new DocumentService(frontEnd);

        System.out.println("GetTotalAmount: " + documentService.getTotalAmount());
        System.out.println("GetAmountByGroup: " + documentService.getAmountByGroup(2));
    }

    private static void awaitTwoClusterNodesToJoin(ActorRef cloudService) {
        Inbox inbox = Inbox.create(system);
        cloudService.tell(new CloudServiceMsg.SubscribeToClusterEvents(), inbox.getRef());
        for (; ; ) {
            Object msg = inbox.receive(Properties.FINITE_DURATION);
            if (msg instanceof CloudServiceMsg.MemberUp) {
                if (((CloudServiceMsg.MemberUp) msg).totalNodesCount >= 2) {
                    break;
                }
            }
        }
    }

    private static AllDocumentsToBeSaved allDocumentsToBeSaved(int documentsCount, Inbox saveAckInbox) {
        return new AllDocumentsToBeSaved(documentsCount, saveAckInbox);
    }

    static class AllDocumentsToBeSaved {
        AtomicInteger documentsCount;
        Inbox saveAckInbox;

        AllDocumentsToBeSaved(int documentsCount, Inbox saveAckInbox) {
            this.documentsCount = new AtomicInteger(documentsCount);
            this.saveAckInbox = saveAckInbox;
        }

        void await() {
            while (documentsCount.get() != 0) {
                System.out.println(documentsCount.get() + " chunks left to save");
                Object msg = saveAckInbox.receive(Properties.FINITE_DURATION);
                if (msg instanceof RepositoryMsg.Ack) {
                    documentsCount.decrementAndGet();
                }
            }
        }
    }

}
