package domain;

import cloud.functions.MapFunction;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.Collection;

public class DocumentServiceTest {

    @Test
    public void testMapFun() throws Exception {
        MapFunction<?, ?> predicate = getMapFunction();

        ObjectOutputStream oos = new ObjectOutputStream(new ByteArrayOutputStream());

        oos.writeObject(predicate);
    }

    private MapFunction<Collection<Document>, BigDecimal> getMapFunction() {
        return DocumentService.mapFun(doc -> true);
    }

}